<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \Aula\Entities\Project::truncate();

        factory(\Aula\Entities\User::class)->create([
            'name'              => 'Felipe',
            'email'             => 'freedfelipe@gmail.com',
            'password'          => bcrypt(1234),
            'remember_token'    => str_random(10),
        ]);

        factory(\Aula\Entities\User::class, 10)->create();
    }
}
