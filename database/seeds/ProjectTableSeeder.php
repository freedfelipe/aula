<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \Aula\Entities\Project::truncate();
        factory(\Aula\Entities\Project::class, 10)->create();
    }
}
