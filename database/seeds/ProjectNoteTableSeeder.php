<?php

use Illuminate\Database\Seeder;

class ProjectNoteTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \Aula\Entities\Project::truncate();
        factory(\Aula\Entities\ProjectNote::class, 50)->create();
    }
}
