<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // \Aula\Entities\Client::truncate();
        factory(\Aula\Entities\Client::class, 10)->create();
    }
}
