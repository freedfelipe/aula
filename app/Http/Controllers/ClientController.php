<?php

namespace Aula\Http\Controllers;

use Aula\Repositories\ClientRepository;
use Aula\Services\ClientService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class ClientController extends Controller
{
    /**
     *
     * @var ClientRepository
     */
    private $repository;
    /**
     *
     * @var ClientService
     */
    private $service;

    public function __construct(ClientRepository $repository, ClientService $service)
    {
        $this->repository = $repository;
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try{
            return $this->repository->all();
        }catch(ModelNotFoundException $e){
            return ['error' => 'No data for list'];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        try{
            return $this->repository->find($id);
        }catch(ModelNotFoundException $e){
            return ['error' => 'Not Found'];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        try{
            return $this->service->update($request->all(), $id);
        }catch(ModelNotFoundException $e){
            return ['error' => 'Unable to update the data'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        try{
            $this->repository->delete($id);
        }catch(QueryException $e){
            return ['error' => 'Unable to delete the data'];
        }
    }
}
