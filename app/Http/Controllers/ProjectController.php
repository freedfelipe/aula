<?php

namespace Aula\Http\Controllers;

use Aula\Repositories\ProjectRepository;
use Aula\Services\ProjectService;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class ProjectController extends Controller
{
    /**
     *
     * @var ProjectRepository
     */
    private $repository;
    /**
     *
     * @var ProjectService
     */
    private $service;

    public function __construct(ProjectRepository $repository, ProjectService $service)
    {
        $this->repository   = $repository;
        $this->service      = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        try{
            return $this->repository->with(['owner', 'client'])->findWhere(['owner_id' => \Authorizer::getResourceOwnerId()]);
        }catch(ModelNotFoundException $e){
            return ['error' => 'No data for list'];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        if(!$this->checkProjectPermissions($id)){
            return ['error' => 'Access Forbidden'];
        }

        try{
            return $this->repository->with(['owner', 'client'])->find($id);
        }catch(ModelNotFoundException $e){
            return ['error' => 'Not Found'];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        if(!$this->checkProjectPermissions($id)){
            return ['error' => 'Access Forbidden'];
        }

        try{
            return $this->service->update($request->all(), $id);
        }catch(ModelNotFoundException $e){
            return ['error' => 'Unable to update the data'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        if(!$this->checkProjectPermissions($id)){
            return ['error' => 'Access Forbidden'];
        }

        try{
            $this->repository->delete($id);
        }catch(QueryException $e){
            return ['error' => 'Unable to delete the data'];
        }
    }

    private function checkProjectOwner($projectId)
    {
        $user_id    = \Authorizer::getResourceOwnerId();

        return $this->repository->isOwner($projectId, $user_id);
    }

    private function checkProjectMember($projectId)
    {
        $user_id    = \Authorizer::getResourceOwnerId();

        return $this->repository->hasMember($projectId, $user_id);
    }

    private function checkProjectPermissions($projectId)
    {
        if($this->checkProjectOwner($projectId) || $this->checkProjectMember($projectId)){
            return true;
        }

        return false;
    }
}
