<?php

namespace Aula\Http\Controllers;

use Aula\Repositories\ProjectNoteRepository;
use Aula\Services\ProjectNoteService;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;

class ProjectNoteController extends Controller
{
    /**
     *
     * @var ProjectRepository
     */
    private $repository;
    /**
     *
     * @var ProjectService
     */
    private $service;

    public function __construct(ProjectNoteRepository $repository, ProjectNoteService $service)
    {
        $this->repository   = $repository;
        $this->service      = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($id)
    {
        try{
            return $this->repository->findWhere(['project_id' => $id]);
        }catch(ModelNotFoundException $e){
            return ['error' => 'No data for list'];
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        return $this->service->create($request->all());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id, $noteId)
    {
        try{
            return $this->repository->findWhere(['project_id' => $id, 'id' => $noteId]);
        }catch(ModelNotFoundException $e){
            return ['error' => 'Not Found'];
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id, $noteId)
    {
        try{
            return $this->service->update($request->all(), $id);
        }catch(ModelNotFoundException $e){
            return ['error' => 'Unable to update the data'];
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id, $noteId)
    {
        try{
            $this->repository->delete($noteId);
        }catch(QueryException $e){
            return ['error' => 'Unable to delete the data'];
        }
    }
}
