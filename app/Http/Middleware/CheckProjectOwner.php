<?php

namespace Aula\Http\Middleware;

use Closure;
use Aula\Repositories\ProjectRepository;

class CheckProjectOwner
{
    /**
     *
     * @var ProjectRepository
     */
    private $repository;

    public function __construct(ProjectRepository $repository)
    {
        $this->repository   = $repository;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user_id    = \Authorizer::getResourceOwnerId();
        $projectId  = $request->project;

        if($this->repository->isOwner($projectId, $user_id) == false){
            return ['error' => 'Access Forbidden'];
        }

        return $next($request);
    }
}
