<?php

namespace Aula\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ProjectNoteRepository
 * @package namespace Aula\Repositories;
 */
interface ProjectNoteRepository extends RepositoryInterface
{
    //
}
