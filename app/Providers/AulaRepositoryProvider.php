<?php

namespace Aula\Providers;

use Illuminate\Support\ServiceProvider;

class AulaRepositoryProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            \Aula\Repositories\ClientRepository::class,
            \Aula\Repositories\ClientRepositoryEloquent::class
        );

        $this->app->bind(
            \Aula\Repositories\ProjectRepository::class,
            \Aula\Repositories\ProjectRepositoryEloquent::class
        );

        $this->app->bind(
            \Aula\Repositories\ProjectNoteRepository::class,
            \Aula\Repositories\ProjectNoteRepositoryEloquent::class
        );
    }
}
