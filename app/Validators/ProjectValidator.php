<?php

namespace Aula\Validators;

use Prettus\Validator\LaravelValidator;

/**
 * @class ProjectValidator
 */
class ProjectValidator extends LaravelValidator
{
    protected $rules = [
        'owner_id'      => 'required|integer',
        'client_id'     => 'required|integer',
        'name'          => 'required|max:255',
        'description'   => 'required',
        'status'        => 'required',
        'progress'      => 'required',
        'due_date'      => 'required',
    ];
}
