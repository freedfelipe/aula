<?php

namespace Aula\Presenters;

use Aula\Transformers\UserTransformer;
use Prettus\Repository\Presenter\FractalPresenter;

class ProjectMemberPresenter extends FractalPresenter
{
    public function getTransformer()
    {
        return new ProjectMemberTransformer;
    }
}
